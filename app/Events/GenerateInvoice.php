<?php

namespace App\Events;

use App\Models\ClientProducts;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GenerateInvoice
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $clientProducts;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ClientProducts  $clientProducts)
    {
        $this->clientProducts = $clientProducts;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
    }
}
