<?php

namespace App\Http\Controllers;

use App\Events\GenerateInvoice;
use App\Models\ClientProducts;
use App\Models\Invoices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
class AdminController extends Controller
{

    public function index(){
        $invoices = User::select(
            'users.id',
            'users.name as client_name',
            DB::raw('SUM(invoices.price) as price'),
            DB::raw('SUM(invoices.tax) as tax'),
            DB::raw('SUM(invoices.price * invoices.tax / 100) as price_tax'),
            DB::raw('SUM(invoices.price + (invoices.price * invoices.tax / 100)) as total_price'),
            )
        ->join('client_products', 'users.id', '=', 'client_products.client_id')
        ->join('products', 'products.id', '=', 'client_products.product_id')
        ->join('invoices', 'invoices.client_products_id', '=', 'client_products.id')
        ->where('client_products.status_invoice', true)
        ->groupBy('users.id')
        ->get();

        return view('admin.index')->with('invoices', $invoices);
    }

    public function invoiceGenerate(){

        $clientProducts = ClientProducts::select(
            'client_products.id',
            'products.price',
            'products.tax',
            'client_products.status_invoice'
        )
        ->join('products', 'products.id', '=', 'client_products.product_id')
        ->where('client_products.status_invoice', false)
        ->get();
        if(empty($clientProducts)){
            return redirect()->back()->with('error', 'No hay productos para generar la factura');
        }
        foreach ($clientProducts as $clientProduct) {
            $clientProduct->status_invoice = true;
            $clientProduct->save();
        }
        return redirect()->back()->with('success', 'Facturas generadas correctamente');
    }
}
