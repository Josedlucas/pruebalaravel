<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClientProductsRequest;
use App\Http\Requests\UpdateClientProductsRequest;
use App\Models\ClientProducts;

class ClientProductsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClientProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function buy(StoreClientProductsRequest $request)
    {
        try {
            $clientProduct = ClientProducts::create($request->all());
            if ($clientProduct) {
                return redirect()->route('products.index')->with('success', 'Producto comprado con éxito');
            }
            return redirect()->route('products.index')->with('error', 'No se pudo comprar el producto');
        } catch (\Exception $e) {
            return redirect()->route('products.index')->with('error', 'Error al comprar el producto');
        }
    }

}
