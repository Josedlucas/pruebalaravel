<?php

namespace App\Listeners;

use App\Events\GenerateInvoice;
use App\Models\Invoices;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GenerateInvoice $event, Invoices $invoices)
    {
        foreach ($event->clientProducts as $clientProduct) {
            $invoices->create([
                'client_products_id' => $clientProduct->id,
                'price' => $clientProduct->product->price,
                'tax' => $clientProduct->product->tax,
            ]);
        }
        return true;
    }
}
