<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class ClientProducts extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'client_id',
        ];
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->client_id = Auth::user()->id;
        });
        static::updating(function($model){
            Invoices::create([
                'client_products_id' => $model->id,
                'price' => $model->price,
                'tax' => $model->tax,
            ]);
        });
    }

    // relations
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function client(){
        return $this->belongsTo(User::class, 'client_id');
    }
    public function invoices(){
        return $this->hasMany(Invoices::class, 'client_products_id');
    }
}
