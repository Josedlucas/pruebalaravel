<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_products_id',
        'price',
        'tax',
    ];

    // relations
    public function client_product(){
        return $this->belongsTo(ClientProducts::class, 'client_products_id');
    }
}
