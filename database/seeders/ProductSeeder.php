<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ceate 5 products
        $pruducts = [
            [
                'name' => 'Product 1',
                'description' => 'Product 1 description',
                'price' => '123.45',
                'image' => 'https://picsum.photos/id/237/200/300',
                'tax' => '5',
            ],
            [
                'name' => 'Product 2',
                'description' => 'Product 2 description',
                'price' => '45.65',
                'image' => 'https://picsum.photos/id/238/200/300',
                'tax' => '15',
            ],
            [
                'name' => 'Product 3',
                'description' => 'Product 3 description',
                'price' => '39.73',
                'image' => 'https://picsum.photos/id/239/200/300',
                'tax' => '12',
            ],
            [
                'name' => 'Product 4',
                'description' => 'Product 4 description',
                'price' => '250.00',
                'image' => 'https://picsum.photos/id/231/200/300',
                'tax' => '8',
            ],
            [
                'name' => 'Product 5',
                'description' => 'Product 5 description',
                'price' => '59.35',
                'image' => 'https://picsum.photos/id/232/200/300',
                'tax' => '10',
            ]
        ];

        foreach ($pruducts as $product) {
            Product::create($product);
        }

    }
}
