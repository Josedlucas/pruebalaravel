<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ceate 2 users
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@tienda.com',
                'password' => Hash::make('12345678'),
                'rol' => 'admin',
            ],
            [
                'name' => 'Cliente 1',
                'email' => 'cliente1@tienda.com',
                'password' => Hash::make('12345678'),
                'rol' => 'cliente',
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
