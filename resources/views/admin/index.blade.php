@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if($errors->any())
                <div class="alert alert-danger">TEse</div>
            @endif
            <div class="col-lg-10 col-md-12  card  py-3">
                @if(!empty($products))
                    <section>
                        <div class="row justify-content-center">

                        </div>
                    </section>
                @else
                    <section class="col-md-8 mx-auto">
                        <div class="row justify-content-center">
                            <div class="d-flex justify-content-between mb-3 mt-5 px-0">
                                <h4>List of generated invoices</h4>
                                <a href="{{route('admin.invoice_generate')}}" class="btn btn-primary">Generate invoices</a>
                            </div>
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>Price</th>
                                    <th>Tax</th>
                                    <th>tax collected</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>{{$invoice->client_name}}</td>
                                        <td>${{$invoice->price}}</td>
                                        <td>%{{$invoice->tax}}</td>
                                        <td>${{($invoice->price * $invoice->tax / 100)}}</td>
                                        <td>${{$invoice->price + ($invoice->price * $invoice->tax / 100)}}</td>
                                        <td>
                                            <div class="modal fade" id="exampleModal{{$loop->index}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$loop->index}}" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel{{$loop->index}}">Invoices Detail</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Client: {{$invoice->client_name}}</p>
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <th>Products</th>
                                                                    <th>Price</th>
                                                                    <th>Tax</th>
                                                                    <th>SubTotal</th>
                                                                </thead>
                                                                <tbody>
                                                                @php($total = 0)
                                                                @php($tax = 0)
                                                                @php($price = 0)
                                                                @foreach($invoice->client_products as $client_product)
                                                                    @foreach($client_product->invoices as $invoice)
                                                                        <tr>
                                                                            <td>{{$client_product->product->name}}</td>
                                                                            <td>${{$invoice->price}}</td>
                                                                            <td>%{{$invoice->tax}}</td>
                                                                            <td>${{$invoice->price * $invoice->tax / 100}}</td>
                                                                        </tr>
                                                                        @php($total += $invoice->price * $invoice->tax / 100)
                                                                        @php($tax += $invoice->tax)
                                                                        @php($price += $invoice->price)
                                                                    @endforeach
                                                                @endforeach
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td>Total</td>
                                                                        <td>${{$price}}</td>
                                                                        <td>%{{$tax}}</td>
                                                                        <td>${{$total}}</td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{$loop->index}}">
                                                Veiw detail
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </section>
                @endif
            </div>
        </div>
    </div>
@endsection
