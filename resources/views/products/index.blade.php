@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if($errors->any())
                <div class="alert alert-danger">TEse</div>
            @endif
            <div class="col-lg-10 col-md-12">
                @if(!empty($products))
                    <section>
                        <div class="row justify-content-center">
                            @foreach($products as $product)
                                <form class="col-lg-3 col-md-2 col-12 card m-3" method="post" action="{{route('cliente.buy')}}">
                                    @csrf
                                    <img class="card-img-top" src="{{$product->image}}" height="200" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->name}}</h5>
                                        <p class="card-text mb-0">Precio: ${{$product->price}}</p>
                                        <p class="card-text">Impuesto: %{{$product->tax}}</p>
                                        <h6>Total: ${{$product->price + ($product->price * $product->tax / 100)}}</h6>
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <button type="submit" class="btn btn-primary col-12">Comprar</button>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </section>
                @else
                    <section>
                        <div class="row justify-content-center">
                           <h4>No hay productos disponibles</h4>
                        </div>
                    </section>
                @endif
            </div>
        </div>
    </div>
@endsection
