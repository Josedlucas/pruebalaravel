<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth:client'], function () {
    Route::get('/products', "ProductController@index")->name('products.index');
    Route::post('/buy', "ClientProductsController@buy")->name('cliente.buy');
});

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/admin', "AdminController@index")->name('admin.index');
    Route::get('/invoce-generate', "AdminController@invoiceGenerate")->name('admin.invoice_generate');
});
